package nodeinfo

type (
	// NodeProtocol defines the supported outbound protocols
	NodeProtocol string

	// NodeService defines the supported inbound services
	NodeService string
)

// Protocols that can be supported by this node.
const (
	ProtocolActivityPub NodeProtocol = "activitypub"
	ProtocolBuddyCloud               = "buddycloud"
	ProtocolDFRN                     = "dfrn"
	ProtocolDisaspora                = "diaspora"
	ProtocolLibertree                = "libertree"
	ProtocolOStatus                  = "ostatus"
	ProtocolPumpIO                   = "pumpio"
	ProtocolTent                     = "tent"
	ProtocolXMPP                     = "xmpp"
	ProtocolZot                      = "zot"
)

// Services that can be supported (inbound or outbound) by this node's API.
const (
	ServiceAtom      NodeService = "atom1.0"
	ServiceGNUSocial             = "gnusocial"
	ServiceIMAP                  = "imap"
	ServicePnut                  = "pnut"
	ServicePOP3                  = "pop3"
	ServicePumpIO                = "pumpio"
	ServiceRSS                   = "rss2.0"
	ServiceTwitter               = "twitter"
	ServiceTumblr                = "tumblr"
)

// Config holds the node info configuration
type Config struct {
	BaseURL   string
	InfoURL   string
	Protocols []NodeProtocol
	Services  Services
	Software  SoftwareInfo
}

type (
	// NodeInfo includes all required node info.
	NodeInfo struct {
		Metadata          interface{}    `json:"metadata"`
		OpenRegistrations bool           `json:"openRegistrations"`
		Protocols         []NodeProtocol `json:"protocols"`
		Services          Services       `json:"services"`
		Software          SoftwareInfo   `json:"software"`
		Usage             Usage          `json:"usage"`
		Version           string         `json:"version"`
	}

	// PleromaMetadata for nodeinfo. Properties are based on what Pleroma uses.
	//
	// From the spec: Free form key value pairs for software specific values.
	// Clients should not rely on any specific key present.
	PleromaMetadata struct {
		NodeName                  string            `json:"nodeName,omitempty"`
		NodeDescription           string            `json:"nodeDescription,omitempty"`
		Private                   bool              `json:"private,omitempty"`
		Software                  *SoftwareMeta     `json:"software,omitempty"`
		AccountActivationRequired bool              `json:"accountActivationRequired,omitempty"`
		Features                  []string          `json:"features,omitempty"`
		StaffAccounts             []string          `json:"staffAccounts,omitempty"`
		RestrictedNicknames       []string          `json:"RestrictedNicknames,omitempty"`
		PostFormats               []string          `json:"postFormats,omitempty"`
		InvitesEnabled            bool              `json:"invitesEnabled,omitempty"`
		MailerEnabled             bool              `json:"mailerEnabled,omitempty"`
		SkipThreadContainment     bool              `json:"skipThreadContainment,omitempty"`
		UploadLimits              map[string]uint32 `json:"uploadLimits,omitempty"`

		Federation struct {
			Enabled              bool     `json:"enabled,omitempty"`
			Exclusions           bool     `json:"exclusions,omitempty"`
			MRFPolicies          []string `json:"mrf_policies,omitempty"`
			QuarentinedInstances []string `json:"quarentined_instances,omitempty"`
		} `json:"federation,omitempty"`

		FieldsLimits struct {
			MaxFields       uint16 `json:"maxFields,omitempty"`
			MaxRemoteFields uint16 `json:"maxRemoteFields,omitempty"`
			NameLength      uint16 `json:"nameLength,omitempty"`
			ValueLength     uint16 `json:"valueLength,omitempty"`
		} `json:"fields_limits,omitempty"`

		PollLimits struct {
			MaxExpiration  uint32 `json:"max_expiration,omitempty"`
			MaxOptionChars uint16 `json:"max_option_chars,omitempty"`
			MaxOptions     uint16 `json:"max_options,omitempty"`
			MinExpiration  uint16 `json:"min_expiration,omitempty"`
		} `json:"poll_limits,omitempty"`
	}

	// Services for the nodeinfo.
	Services struct {
		Inbound  []NodeService `json:"inbound"`
		Outbound []NodeService `json:"outbound"`
	}

	// SoftwareInfo of the nodeinfo.
	SoftwareInfo struct {
		// Name (canonical) of this server software.
		Name string `json:"name"`
		// Version of this server software.
		Version string `json:"version"`
	}

	// SoftwareMeta of the nodeinfo.
	SoftwareMeta struct {
		HomePage string `json:"homepage"`
		GitHub   string `json:"github"`
		Follow   string `json:"follow"`
	}

	// Usage is usage statistics for this server.
	Usage struct {
		Users         UsageUsers `json:"users"`
		LocalPosts    int        `json:"localPosts,omitempty"`
		LocalComments int        `json:"localComments,omitempty"`
	}

	// UsageUsers is user statistics for this server.
	UsageUsers struct {
		Total          int `json:"total,omitempty"`
		ActiveHalfYear int `json:"activeHalfyear,omitempty"`
		ActiveMonth    int `json:"activeMonth,omitempty"`
	}
)
