package nodeinfo

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/bakyun/jrd"
)

// NodeInfoPath defines the default path of the nodeinfo handler.
const NodeInfoPath = "/.well-known/nodeinfo"

type discoverInfo struct {
	Links []jrd.Link `json:"links"`
}

// NodeInfoDiscover is the http.HandlerFunc for the nodeinfo discover endpoint.
func (s *Service) NodeInfoDiscover(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	i := discoverInfo{
		Links: []jrd.Link{
			{
				Rel:  profile,
				Href: s.InfoURL,
			},
		},
	}

	body, err := json.Marshal(i)
	if err != nil {
		log.Printf("Unable to marshal nodeinfo discovery: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)

	if _, err := w.Write(body); err != nil {
		log.Printf("Unable to write body: %v", err)
		return
	}
}

// NodeInfo is the http.HandlerFunc for the nodeinfo endpoint.
func (s *Service) NodeInfo(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "application/json; profile="+profile+"#")

	i, err := s.BuildInfo()
	if err != nil {
		log.Printf("Unable to build nodeinfo: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	body, err := json.Marshal(i)
	if err != nil {
		log.Printf("Unable to marshal nodeinfo: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)

	if _, err := w.Write(body); err != nil {
		log.Printf("Unable to write body: %v", err)
		return
	}
}
