package nodeinfo

const (
	profileVer = "2.0"
	profile    = "http://nodeinfo.diaspora.software/ns/schema/" + profileVer
)

// Service is the nodeinfo service description
type Service struct {
	InfoURL string
	Info    NodeInfo
	Config  Config

	resolver Resolver
}

// NewService creates a new service description
func NewService(cfg Config, r Resolver) *Service {
	return &Service{
		Config:  cfg,
		InfoURL: cfg.BaseURL + cfg.InfoURL,
		Info: NodeInfo{
			Protocols: cfg.Protocols,
			Services:  cfg.Services,
			Software:  cfg.Software,
		},
		resolver: r,
	}
}

// BuildInfo builds the nodeinfo
func (s Service) BuildInfo() (*NodeInfo, error) {
	var err error
	ni := &s.Info
	ni.OpenRegistrations, err = s.resolver.IsOpenRegistration()
	if err != nil {
		return nil, err
	}
	ni.Usage, err = s.resolver.Usage()
	if err != nil {
		return nil, err
	}
	ni.Metadata, err = s.resolver.Metadata()
	if err != nil {
		return nil, err
	}
	ni.Version = profileVer
	return ni, nil
}
